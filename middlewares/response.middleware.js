const responseMiddleware = (req, res, next) => {
   // DONE: Implement middleware that returns result of the query
//--
    if(req.invalid!==undefined && req.invalid.length > 0){
        message = req.invalid.reduce((accumulator, currentValue)=>{
            return accumulator + currentValue +"; ";
        }, "");
        res.status(400).send({
            error: true,
            message
        });
    } else if(req.result){
        res.status(200).send(req.result);
    } else {
        if(req.userExist === true){
            res.status(404).send({
                error: true,
                message: "this user already exists"
            });
        } else if(req.userExist === false){
            res.status(404).send({
                error: true,
                message: "this user not exists"
            });
        }
        if(req.fighterExist === true){
            res.status(404).send({
                error: true,
                message: "this fighter already exists"
            });
        } else if(req.fighterExist === false){
            res.status(404).send({
                error: true,
                message: "this fighter not exists"
            });
        }
    }
//--
    // next();
}

exports.responseMiddleware = responseMiddleware;