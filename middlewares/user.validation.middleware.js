const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // DONE: Implement validatior for user entity during creation
    //--
    const newUser = req.body;
    req.invalid = [];
    if (newUser) {
        const stringMessage = "does not match the format";
        if(typeof newUser.firstName !== typeof user.firstName){
            req.invalid.push(`first name ${stringMessage}`);
        }
        if(typeof newUser.lastName !== typeof user.lastName){
            req.invalid.push(`last name ${stringMessage}`);
        }
        if(typeof newUser.email !== typeof user.email || 
            newUser.email.slice(newUser.email.length-10, newUser.email.length) !== "@gmail.com"){
                req.invalid.push(`email  ${stringMessage}`);
        } 
        if(typeof newUser.phoneNumber !== typeof user.phoneNumber || 
            newUser.phoneNumber.slice(0, 4) !== "+380"){
                req.invalid.push(`phone number  ${stringMessage}`);
        } 
        if(typeof newUser.password !== typeof user.password || 
            newUser.password.length < 3){
                req.invalid.push(`password  ${stringMessage}`);
        } 
    } else req.invalid.push('Object newUser is missing');
    //--
    next();
}

const updateUserValid = (req, res, next) => {
    // DONE: Implement validatior for user entity during update
    //--
    const updateUser = req.body;
    req.invalid = [];
    if (updateUser) {
        const stringMessage = "does not match the format";
        if(updateUser.firstName!==undefined && typeof updateUser.firstName !== typeof user.firstName){
            req.invalid.push(`first name ${stringMessage}`);
        }
        if(updateUser.lastName!==undefined && typeof updateUser.lastName !== typeof user.lastName){
            req.invalid.push(`last name ${stringMessage}`);
        }
        if(updateUser.email!==undefined && (typeof updateUser.email !== typeof user.email || 
            updateUser.email.slice(updateUser.email.length-10, updateUser.email.length) !== "@gmail.com")){
                req.invalid.push(`email  ${stringMessage}`);
        } 
        if(updateUser.phoneNumber!==undefined && (typeof updateUser.phoneNumber !== typeof user.phoneNumber || 
            updateUser.phoneNumber.slice(0, 4) !== "+380")){
                req.invalid.push(`phone number  ${stringMessage}`);
        } 
        if(updateUser.password!==undefined && (typeof updateUser.password !== typeof user.password || 
            updateUser.password.length < 3)){
                req.invalid.push(`password  ${stringMessage}`);
        } 
    } else req.invalid.push('Object updateUser is missing');
    //--
    next();
}


exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;