const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // DONE: Implement login action (get the user if it exist with entered credentials)
        const result = AuthService.login(req.body);console.log(result, "result -> login post");//**************** */
        req.result = result;
    } catch (err) {
        if(!req.invalid){
           req.invalid=[];
        } 
        req.invalid.push(err.message);
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;