const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// DONE: Implement route controllers for user

//--
router.post('/', createUserValid, function(req, res, next) {
    if(req.invalid===undefined || req.invalid.length===0){
        const result = UserService.saveInRepository(req.body);
        if (result) {
            req.result = result;
        } else {
            req.userExist = true;
        }
    }
    next();
}, responseMiddleware);
router.put('/:id', updateUserValid, function(req, res, next) {
    if(req.invalid===undefined || req.invalid.length===0){
        const result = UserService.updateInRepository(req.params["id"], req.body);
        if (result.id) {
            req.result = result;
        } else {
            req.userExist = false;
        }
    }
    next();
}, responseMiddleware);
router.delete('/:id', function(req, res, next) {
    const result = UserService.deleteFromRepository(req.params["id"]);
    if (result) {
        req.result = result;
    } else {
        req.userExist = false;
    }    
    next();
}, responseMiddleware);
router.get('/:id', function(req, res, next) {
    const result = UserService.search({"id":req.params["id"]});
    if (result) {
        req.result = result;
    } else {
        req.userExist = false;
    }
    next();
}, responseMiddleware);
router.get('/', function(req, res, next) {
    const result = UserService.getAllFromRepository();
    req.result = result;
    next();
}, responseMiddleware);
//--

module.exports = router;